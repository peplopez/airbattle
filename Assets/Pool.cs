﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class that represent any GameObject that can be inside the pool
[System.Serializable]
public class PoolItem
{
    public GameObject mPrefab;
    public int mAmount;
    public bool mExpandable;
}

public class Pool : MonoBehaviour
{
    /*
     Generic Pool for any GameObject identified by tag stablished in the prefab.
     */

    public static Pool Singleton;
    public List<PoolItem> mItems;
    public List<GameObject> mPooledItems;

    void Awake()
    {
        //Singleton pattern used
        Singleton = this;
    }

    //Access pool object by tag
    public GameObject Get(string tag)
    {
        for (int i = 0; i < mPooledItems.Count; i++)
        {
            if (!mPooledItems[i].activeInHierarchy && mPooledItems[i].tag == tag)
            {
                return mPooledItems[i];
            }
        }

        foreach (PoolItem item in mItems)
        {
            if (item.mPrefab.tag == tag && item.mExpandable)
            {
                GameObject obj = Instantiate(item.mPrefab);
                obj.SetActive(false);
                mPooledItems.Add(obj);
                return obj;
            }
        }

        return null;
    }

    void Start()
    {
        //fill of the pool at the beginning. No more objects are added later
        mPooledItems = new List<GameObject>();
        foreach (PoolItem item in mItems)
        {
            for (int i = 0; i < item.mAmount; i++)
            {
                GameObject obj = Instantiate(item.mPrefab);
                obj.SetActive(false);
                mPooledItems.Add(obj);
                obj.transform.SetParent(this.gameObject.transform);
            }
        }
    }

}
