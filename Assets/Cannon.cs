﻿using UnityEngine;

public class Cannon : MonoBehaviour
{
    //Reference to the sprites provided
    public Sprite[] mSprites;
    public Transform[] mSpawnPoints;

    private readonly float INITIAL_CANNON_Y = -3.33f;    

    //a flag to not fire in several updates()
    bool mFiringBullet;
    public enum ShootingMode
    {
        Degree30,
        Degree60,
        Degree90
    }

    private ShootingMode mCurrentShootingMode = ShootingMode.Degree60;

    private SpriteRenderer mSpriteRendererComponent;

    void SetCannonPosition()
    {
        // Setting the cannon x depending on the aspect ratio.
        Debug.Log("Screen Width : " + Screen.width);
        float startCannonX = Screen.width / 4;
        Debug.Log("Cannon x is : " + startCannonX);
        Vector3 screenCannonPosition = new Vector3(startCannonX, 0, 0);
        Vector3 finalPosition = Camera.main.ScreenToWorldPoint(screenCannonPosition);
        finalPosition = new Vector3(finalPosition.x, INITIAL_CANNON_Y);
        transform.position = finalPosition;
    }

    void Start()
    {
        SetCannonPosition();
        
        mSpriteRendererComponent = GetComponent<SpriteRenderer>();
        if (mSpriteRendererComponent == null)
            Debug.LogError("No SpriteRenderer component in cannon");

        mFiringBullet = false;
    }

    
    void Update()
    {
        //Input control for cannon inclination.

        //Avoid of innecesary constant array access.
        //If we are already in an inclination we don't set again and again the sprite while the same key is pressed

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (mCurrentShootingMode != ShootingMode.Degree90)
            {
                mCurrentShootingMode = ShootingMode.Degree90;
                mSpriteRendererComponent.sprite = mSprites[(int) ShootingMode.Degree90 ];
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                if (mCurrentShootingMode != ShootingMode.Degree30)
                {
                    mCurrentShootingMode = ShootingMode.Degree30;
                    mSpriteRendererComponent.sprite = mSprites[(int)ShootingMode.Degree30];
                }
            }
            else
            {
                if (mCurrentShootingMode != ShootingMode.Degree60)
                {
                    mCurrentShootingMode = ShootingMode.Degree60;
                    mSpriteRendererComponent.sprite = mSprites[(int)ShootingMode.Degree60];
                }
            }
        }

        if ( Input.GetKey(KeyCode.Space) )
        {
            if (!mFiringBullet)
            {
                mFiringBullet = true;
                FireBullet();
            }
        }
        else
        {
            mFiringBullet = false;
        }
    }
    
    void FireBullet()
    {
        //Get an instance from pool and activate it
        GameObject a = Pool.Singleton.Get("bullet");
        if (a != null)
        {
            //a.transform.position = this.transform.position;
            a.transform.position = mSpawnPoints[(int)mCurrentShootingMode].position;
            a.transform.rotation = Quaternion.Euler(0, 0, BulletAngle());
            a.SetActive(true);
        }        
    }

    float BulletAngle()
    {
        float angle = 0;
        switch (mCurrentShootingMode)
        {
            case ShootingMode.Degree30:
                angle = 30;
                break;

            case ShootingMode.Degree60:
                angle = 60;
                break;

            case ShootingMode.Degree90:
                angle = 90;
                break;
        }
        return angle;
    }
}
