﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{    
    public static GameManager Singleton;
    private AudioSource mAudioSourceComponent;
    public AudioClip mExplosionClip;

    //Prefabs
    public GameObject mPool;
    public GameObject mCannon;

    private GameObject mCannonInstance;

    public int mScore;
    public int mHighScore;
    public int mPointPerPlane;

    public float mRemainingTime;

    bool mInGameMode;
    private int mFlyingPlanes;
    private float sInitialPlaneX;
    readonly float PLANE_REFERENCE_POSITION_Y = 4.05f;
    readonly float PLANE_GAPS = 1.12f;

    public int mMatchDuration = 10;

    #region UI_REFERENCES
    public Text mTimeRemainingText;
    public Text mScoreText;
    public Text mHighScoreText;
    public GameObject mIngameUIGameObject;
    public GameObject mMenuUIGameObject;
    public GameObject mTimeUIGameObject;
    #endregion

    #region COUNT_DOWN
    public int mTimeRemaining;
    
    public bool mIsCountingDown = false;
    public void BeginCountdown()
    {
        if (!mIsCountingDown)
        {
            mIsCountingDown = true;
            mTimeRemaining = mMatchDuration;
            Invoke("_tick", 1f);
        }
    }

    private void _tick()
    {
        mTimeRemaining--;
        if (mTimeRemaining > 0)
        {
            mTimeRemainingText.text = mTimeRemaining.ToString();
            Invoke("_tick", 1f);
        }
        else
        {
            mIsCountingDown = false;

            CheckScores();           
        }
    }
    #endregion 

    void CheckScores()
    {
        if (mScore > mHighScore)
        {
            mHighScore = mScore;
            mHighScoreText.text = mHighScore.ToString();
        }

        ExitGameMode();
    }

    void Start()
    {
        Singleton = this;
        mInGameMode = false;
        sInitialPlaneX = CalculatePlaneInitialX();
        //If they are active in the editor
        HideIngameMenuElements();

        mAudioSourceComponent = GetComponent<AudioSource>();
        if (mAudioSourceComponent == null)
            Debug.LogError("No AudioSource set in GameManager");

        if (mExplosionClip == null)
            Debug.LogError("No AudioClip set in GameManager");
    }

    private void PlayPlaneExplosion()
    {
        mAudioSourceComponent.PlayOneShot(mExplosionClip);
    }
    
    public void PlaneDestroyed()
    {        
        PlayPlaneExplosion();

        //To know if all planes of a formation are destroyed
        mFlyingPlanes--;

        //Adding point to the score
        mScore+= mPointPerPlane;
        mScoreText.text = mScore.ToString();

        Debug.Log("Plane Destroyed");
        Debug.Log("FlyingPlanes: " + mFlyingPlanes);
        if (mFlyingPlanes == 0)
        {
            Debug.Log("Formation Destroyed, Launching new formation");
            Invoke("LaunchFormation", .1f);
        }
    }

    public  float CalculatePlaneInitialX()
    {
        Vector3 leftMargin = new Vector3(0, 0, 0);
        Vector3 initialPlanePosition = Camera.main.ScreenToWorldPoint(leftMargin);
        Debug.Log("Plane x is : " + initialPlanePosition.x);
        return initialPlanePosition.x;
    }    

    #region UI_ELEMENTS_ACTIVATION
    private void HideMainMenuElements()
    {
        mMenuUIGameObject.SetActive(false);
    }

    private void ShowMainMenuElements()
    {
        mMenuUIGameObject.SetActive(true);
    }

    private void HideIngameMenuElements()
    {
        mIngameUIGameObject.SetActive(false);
    }

    private void HideTimeElement()
    {
        mTimeUIGameObject.SetActive(false);
    }

    public void ShowIngameMenuElements()
    {
        mIngameUIGameObject.SetActiveRecursively(true);
    }
    #endregion

    public void BeginGameMode()
    {
        //Coming from Menu, this method set all the game scene prepared for a match
        mInGameMode = true;
        //Reset of the score with the begining of a new round.
        mScore = 0;
        HideMainMenuElements();
        ShowIngameMenuElements();
        
        //If not exists, here I create the pool, but if it exists is inactive and it's necessary to activate it with the inactive pool items inside 
        if (Pool.Singleton == null)
        {
            Instantiate(mPool);
        }
        else
        {
            Pool.Singleton.gameObject.SetActive(true);
        }

        //As the pool, we create the cannon just once
        if (mCannonInstance==null)
            mCannonInstance = Instantiate(mCannon);
        else
            mCannonInstance.SetActive(true);        

        //Count down stablished with config values
        BeginCountdown();
        mTimeRemainingText.text = mMatchDuration.ToString() ;

        //Added a delay to let the pool create all the planes necessary
        Invoke("LaunchFormation", .5f);
    }

    public void ExitGameMode()
    {
        mInGameMode = false;

        //HideIngameMenuElements();
        HideTimeElement();

        ShowMainMenuElements();
        mCannonInstance.SetActive(false);

        //Deactivate Pool, no neccesity to destroy the pool and his childrens
        Pool.Singleton.gameObject.SetActiveRecursively(false);
    }

    void LaunchFormation()
    {
        if (mInGameMode) //in case that the delayed launch occurs in MainMenu (very rare)
        {
            //A formation of 3, 4 or 5 planes
            int launchedPlanes = Random.Range(3, 6);

            for (int i = 0; i < launchedPlanes; i++)
            {
                Vector3 position = new Vector3(sInitialPlaneX, PLANE_REFERENCE_POSITION_Y - i * PLANE_GAPS);
                LaunchPlane(position);
            }
            mFlyingPlanes = launchedPlanes;
        }
    }

    void LaunchPlane(Vector3 position)
    {
        //Obtain a GameObject tagged as plane from the pool
        GameObject a = Pool.Singleton.Get("plane");
        if (a != null)
        {
            a.transform.position = position;                            
            a.SetActive(true);
        }
        else
        {
            Debug.LogError("No planes to Launch");
        }
    }

    //This method is called from ServerConnection component in this GameObject
    public void SetConfig(ServerConnection.Config config )
    {
        mMatchDuration = config.time_limit;
        mHighScore = config.default_high_score;
        mPointPerPlane = config.points_per_Plane;

        mHighScoreText.text = mHighScore.ToString();

        Debug.Log("GameManager.SetConfig() - configuration stablished");
    }
}
