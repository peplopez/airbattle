﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{
    [Range(1f, 10f)]
    public float mSpeed;
    private SpriteRenderer mSpriteRendererComponent;
    private float mInitialX;

    void Start()
    {
        mSpriteRendererComponent = GetComponent<SpriteRenderer>();
        if (mSpriteRendererComponent == null)
            Debug.LogError("No SpriteRenderer component in plane");

        //Store the initial x to reestablish it when the plane dissapear by the right
        mInitialX = transform.position.x;
    }

    void Update()
    {
        //Constant plane movement
        float newX = transform.position.x + mSpeed * Time.deltaTime;        
        transform.position = new Vector3(newX, transform.position.y, transform.position.z);       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            GameManager.Singleton.PlaneDestroyed();            
            //set both objects as inactive to be reused in the pool
            collision.gameObject.SetActive(false);
            this.gameObject.SetActive(false);            
        }
    }

    private void OnBecameInvisible()
    {
        //return to left side of the screen
        transform.position = new Vector3(mInitialX, transform.position.y, transform.position.z); 
    }

}
