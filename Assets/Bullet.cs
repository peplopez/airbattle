﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //I permit to change speed using a slider like with the planes
    [Range(1f, 10f)]
    public float mSpeed = 10;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * mSpeed); 
    }

    private void OnBecameInvisible()
    {        
        //Avoid destruction, just deactivate to be reusable by the pool
        this.gameObject.SetActive(false);
    }
}
