﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ServerConnection : MonoBehaviour
{
    [System.Serializable]
    public class Config
    {
        //Default values applied here
        public int time_limit = 30;
        public int default_high_score = 100;
        public int points_per_Plane = 1;
    }

    void Start()
    {
        //Async call to the backend
        StartCoroutine(GetConfig());
    }

    IEnumerator GetConfig()
    {
        Config config = new Config();
        UnityWebRequest www = UnityWebRequest.Get("http://content.gamefuel.info/api/client_programming_test/air_battle_v1/content/config/config");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Debug.Log("Config stablished with default values");
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            Debug.Log("Config received successfully");
            config = JsonUtility.FromJson<Config>(www.downloadHandler.text);           
        }
        SendMessage("SetConfig", config);
    }
    
}
